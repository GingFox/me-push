<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Application extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_app', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('app_id');
            $table->string('frontend_tech');
            $table->string('backend_tech');
            $table->bigInteger('owner_id')->unsigned()->unique();;
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('owner_id')->references('id')->on('user')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('application');
    }
}
